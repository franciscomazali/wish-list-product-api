﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luizalabs.WishListAPI.Product.Business.Interface
{
    public interface IProduct
    {
        List<Model.Product> GetProducts(int page_size, int page);
        string Create(Model.Product model);
    }
}
