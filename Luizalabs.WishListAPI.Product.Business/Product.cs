﻿using Luizalabs.WishListAPI.Product.Business.Interface;
using Luizalabs.WishListAPI.Product.Firebase;
using Luizalabs.WishListAPI.Product.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using static Luizalabs.WishListAPI.Product.Utils.LinqExtentions;

namespace Luizalabs.WishListAPI.Product.Business
{
    public class Product : IProduct
    {
        public string Create(Model.Product model)
        {
            FireProduct fire = new FireProduct();
            string message = fire.Push(model);

            return message;
        }

        public List<Model.Product> GetProducts(int page_size, int page)
        {
            FireProduct fire = new FireProduct();
            var users = fire.GetProducts();

            if (users != null)
            {
                var result = users.Select(x =>
                {
                    return new Model.Product
                    {
                        id = x.Key,
                        name = x.Value.name
                    };
                });

                result = result.Page(page, page_size).ToList();

                return result.ToList();
            }

            return new List<Model.Product>();
        }
    }
}
