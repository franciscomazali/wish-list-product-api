﻿using FirebaseNetAdmin;
using FirebaseNetAdmin.Configurations.ServiceAccounts;
using FirebaseNetAdmin.Firebase.Commands;
using System.Collections.Generic;

namespace Luizalabs.WishListAPI.Product.Firebase
{
    public class FireProduct
    {
        private readonly FirebaseAdmin client;

        public FireProduct()
        {
            var credentials = new JSONServiceAccountCredentials("key/wishlistapi-luiza-firebase-adminsdk-2tnz1-73493d753c.json");
            client = new FirebaseAdmin(credentials);
        }

        public string Push(Model.Product product)
        {
            var users = client.Database.Ref($"products");
            var result = users.Push(product);

            return result;
        }

        public Dictionary<string, Model.Product> GetProducts()
        {
            var db = client.Database.Ref($"products");
            var products = db.Get<Dictionary<string, Model.Product>>();

            return products;

        }
    }
}
