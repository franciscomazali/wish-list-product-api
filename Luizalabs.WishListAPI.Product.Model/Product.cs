﻿using System;

namespace Luizalabs.WishListAPI.Product.Model
{
    public class Product
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
