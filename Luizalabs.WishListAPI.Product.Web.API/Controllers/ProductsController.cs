﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Luizalabs.WishListAPI.Product.Web.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly Business.Interface.IProduct _bus;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public ProductsController(Business.Interface.IProduct bus, IMapper mapper, ILogger<ProductsController> logger)
        {
            _logger = logger;
            _mapper = mapper;
            _bus = bus;
        }

        // GET products/?page_size=10&page=1
        [HttpGet]
        public ActionResult<IEnumerable<Model.ProductDto>> Get(int page_size = 10, int page = 1)
        {
            _logger.LogInformation($"url: products/{HttpContext.Request.QueryString.Value}");

            try
            {
                var users = _bus.GetProducts(page_size, page);
                var result = _mapper.Map<List<Model.ProductDto>>(users);
                return result;
            }
            catch (AutoMapperMappingException ex)
            {
                _logger.LogError($"Automapper Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Erro ao mapear entidade",
                    code = 500
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Ocorreu um erro interno.",
                    code = 500
                });
            }
        }

        // POST products/
        [HttpPost]

        public IActionResult Post(Model.ProductDto model)
        {
            _logger.LogInformation($"Request: {{name: {model.name}}}");

            try
            {
                var user = _mapper.Map<Product.Model.Product>(model);
                string message = _bus.Create(user);
                return StatusCode(201);
            }
            catch (AutoMapperMappingException ex)
            {
                _logger.LogError($"Automapper Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Erro ao mapear entidade",
                    code = 500
                });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error: {ex.Message} - Stack: {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError, new
                {
                    message = "Ocorreu um erro interno.",
                    code = 500
                });
            }
        }

        [HttpHead]
        public void Options()
        {
            Response.Headers.Add("Access-Control-Allow-Headers", "content-type");
            Response.Headers.Add("Access-Control-Allow-Methods", "GET; POST");
        }
    }
}