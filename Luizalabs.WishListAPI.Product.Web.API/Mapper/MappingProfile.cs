﻿using AutoMapper;

namespace Luizalabs.WishListAPI.Product.Web.API.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product.Model.Product, Model.ProductDto>();
            CreateMap<Model.ProductDto, Product.Model.Product>();
        }
    }
}
