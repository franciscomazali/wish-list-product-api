﻿using System.ComponentModel.DataAnnotations;

namespace Luizalabs.WishListAPI.Product.Web.API.Model
{
    public class ProductDto
    {
        public string id { get; set; }
        [Required(ErrorMessage = "Nome é obrigatório")]
        public string name { get; set; }
    }
}
